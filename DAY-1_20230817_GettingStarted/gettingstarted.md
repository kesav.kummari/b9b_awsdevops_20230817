#### Session Video:
    https://drive.google.com/file/d/11OiHm6ghtmA5tXDsNs4RxtqvUHL43ysZ/view?usp=sharing

#### Getting Started with AWS DevOps

Host Machine :
    - Laptop/Desktop :
        - Windows
        - MacOS

    - Softwares :

        - Browsers : Chrome

        - VCS/SCM : Git 

        - IDE Tool : Visual Studio Code 

    - Account Creation :
        
        - VCS/SCM : Gitlab

        - AWS Account Creation :
            - Root User 
