#### Session Video:
    https://drive.google.com/file/d/1PNHNKtI9akfc5wZRAjbGcnpEOe2Yduw4/view?usp=sharing


Task-1 : Login to AWS as Root User 

Task-2 : Create a Group :
    
    Group Name : OpsWork
        
        Attach Permissions : AdministratorAccess
        
        IAM Users :
            - bob (DevOps)

Task-3 : Login To AWS as IAM user i.e. bob 

        - Region : NV 
        
        - IAM :
            - Create a role i.e. ec2SSMConnect
                - Attach Permissions : AmazonSSMManagedInstanceCore

        - Compute :
            - EC2 :
                - Instances :
                    - Windows 2022
                    - Linux (Amazon Linux) 2023


```
Image : 
OS : Windows
OS version : 2023

Instance Type:
CPU: 1 vcpu
RAM : 1 GB

Hard Disk: 30GB 
Type of Hard Disk : gp2

Keypair: nv_sshkeys

VPC: default & random subnet

Security Group: sg_win 
3389/tcp == 0.0.0.0/0 & ::/0
```

Task-4 : Connect Windows & Linux remotely i.e. from your Host Machine

Task-5 : Connect Windows & Linux through SSM 

Task-6 : Install git, vscode & chrome using powershell on windows

Task-7 : Install git, curl, wget & tree using cli on linux
