#### Session Video:
    https://drive.google.com/file/d/1GeaBK4HMN0PCGfHGWaQaJpr0Azm7rrcm/view?usp=sharing

#### Getting Started with AWS DevOps

Host Machine :
    - Laptop/Desktop :
        - Windows
        - MacOS

    - Softwares :

        - Browsers : Chrome

        - VCS/SCM : Git 

        - IDE Tool : Visual Studio Code 

    - Account Creation :
        
        - VCS/SCM : Gitlab

        - AWS Account Creation :
            - Root User 
                - Create IAM User

#### Download & Install Git on Windows using GUI:

https://git-scm.com/

#### Download & Install Git on Windows using CLI:
```
To install Git on Windows using PowerShell, you can follow these steps:

    Open PowerShell as Administrator: 
        Right-click on the Start button, select "Windows PowerShell (Admin)" from the context menu.

    Install Git using Chocolatey (Recommended):
        Chocolatey is a package manager for Windows. 
        
        It simplifies software installation on Windows using a command-line interface. Here's how you can use Chocolatey to install Git:

    Run the following command in the PowerShell window:

        Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

    After the installation is complete, close and reopen PowerShell as Administrator.

    Install Git Package:
        Once Chocolatey is installed, you can use it to install Git by running the following command:

        choco install git -y
    
        The -y flag will automatically answer "yes" to any prompts during the installation process.

    Verify Git Installation:
        After the installation is complete, you can verify that Git is installed by running:

        git --version
    
    This command should display the installed Git version.

That's it! You've successfully installed Git on Windows using PowerShell and Chocolatey. 
You can now use Git for version control and other tasks.

```