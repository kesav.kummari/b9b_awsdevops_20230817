#### Session Video:
    https://drive.google.com/file/d/112wN8pfLuCYqPnI1GDePCyVOHwt_Hrzs/view?usp=sharing

#### AWS Basics 

    - Created a root account :
        - logged into root account: 
            - selected a specific region : us-east-1 (NV)

            - created IAM user & attached permissions
                - logged into aws as IAM user i.e. kesav 
                    - - selected a specific region : us-east-1 (NV)


#### AWS Global Infrastructure 

    - List of regions :
        32
        
    - List of Datacenters i.e. AZ's :
        102

    - About to launch regions :
        - 4 
    - List of Datacenters i.e. AZ's :
        - 12

    - CDN/POPs: 
        - 450+ Points of Presence
        - 400+ Edge Locations and 13 Regional Edge Caches

#### OS Basics 


#### Download & Install Git on Windows using GUI:

https://git-scm.com/

#### Download & Install Git on Windows using CLI:
```
To install Git on Windows using PowerShell, you can follow these steps:

    Open PowerShell as Administrator: 
        Right-click on the Start button, select "Windows PowerShell (Admin)" from the context menu.

    Install Git using Chocolatey (Recommended):
        Chocolatey is a package manager for Windows. 
        
        It simplifies software installation on Windows using a command-line interface. Here's how you can use Chocolatey to install Git:

    Run the following command in the PowerShell window:

        Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

    After the installation is complete, close and reopen PowerShell as Administrator.

    Install Git Package:
        Once Chocolatey is installed, you can use it to install Git by running the following command:

        choco install git -y
    
        The -y flag will automatically answer "yes" to any prompts during the installation process.

    Verify Git Installation:
        After the installation is complete, you can verify that Git is installed by running:

        git --version
    
    This command should display the installed Git version.

That's it! You've successfully installed Git on Windows using PowerShell and Chocolatey. 
You can now use Git for version control and other tasks.

```

#### To install Visual Studio Code using PowerShell

 You can use the chocolatey package manager. Chocolatey is a popular package manager for Windows that simplifies the installation and management of software packages. Here's how you can install Visual Studio Code using PowerShell and Chocolatey:

Open PowerShell as Administrator:
    Right-click on the PowerShell icon and select "Run as administrator" to ensure you have the necessary permissions to install software.

Install Chocolatey:
    If you haven't already installed Chocolatey, you can do so by running the following command in PowerShell:

    Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

This command will download and execute the Chocolatey installation script.

Install Visual Studio Code:
    Once Chocolatey is installed, you can use it to install Visual Studio Code with the following command:

    choco install vscode
    
    This command will download and install the latest version of Visual Studio Code using Chocolatey.

Launch Visual Studio Code:
    After the installation is complete, you can launch Visual Studio Code by either searching for it in the start menu or running the following command in PowerShell:

    code

    This should launch Visual Studio Code, allowing you to start coding.

    Please note that these steps assume you have administrative privileges on your system. If you encounter any issues during the installation process, you might need to adjust your system settings or consult the Chocolatey documentation for troubleshooting.

Keep in mind that software installation methods and availability can change over time, so it's always a good idea to refer to the official websites or documentation for the most up-to-date information.
```